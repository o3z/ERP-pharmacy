Ngu = Negiup

/**
 * @author Ivan Nguyễn
 * @email ivannguyen.it@gmail.com
 * @create date 2018-05-16 10:50:02
 * @modify date 2018-05-16 10:50:49
 * @desc [description]
 */
let queue = {}
let key = 0;
exports.start = function (req, res, next) {
    // logger.debug('[Tracking middleware] Start ', 'cookie: ' + req.cookies._ga + ' | url: ' + req.protocol + '://' + req.get('host') + req.originalUrl)
    key += 1;
    // queue[key] = setTimeout(function () {
    //     // logger.debug('[Tracking middleware] Write ', 'cookie: ' + req.cookies._ga + ' | url: ' + req.protocol + '://' + req.get('host') + req.originalUrl)
    //             Ngu.model('Tracking')
    //             .create({
    //                 cookie: req.cookies._ngu,
    //                 host: req.get('host'),
    //                 description: req.url,
    //                 referer: req.headers.referer,
    //                 meta: {
    //                     url: req.protocol + '://' + req.get('host') + req.originalUrl
    //                 }
    //             },
    //             function (err, track) {})
    // }, 1000);
    req.tracking = key;
    next();
}
exports.end = function (req, res, next) {
    logger.debug('[Middleware] Tracking end', 'cookie: ' + req.cookies._ga + ' | url: ' + req.protocol + '://' + req.get('host') + req.originalUrl)
    clearTimeout(queue[req.tracking]);
    delete queue[req.tracking];
    next();
}
var winston = require('winston');
var fs = require('fs')
winston.emitErrs = true;
winston.level = config.log.level
// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
var timeFormat = () => moment().toString()

/**
 * Logging Service
 * ==========
 * Error code group: 024
 * Purpose: 
 */

var logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'warn',
            name : 'warn-file',
            filename: config.log.path + 'app-warn.log',
            handleExceptions: true,
            json: false,
            timestamp : timeFormat ,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: true
        }),        
        new winston.transports.File({
            level: 'info',
            name : 'info-file',
            filename: config.log.path + 'app-info.log',
            handleExceptions: true,
            json: false,
            timestamp : timeFormat,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: true
        }),
        new winston.transports.File({
            level: 'debug',
            name : 'debug-file',
            filename:  config.log.path + 'app-debug.log',
            handleExceptions: true,
            json: false,
            timestamp : timeFormat,
            maxsize: 15728640, //15MB
            maxFiles: 5,
            colorize: true
        }), 
        new winston.transports.Console({
            level: 'silly',
            handleExceptions: true,
            json: false,
            timestamp : timeFormat,
            colorize: true
        })
    ],
    exceptionHandlers: [
        new winston.transports.File({
            colorize: true,
            level: 'error',
            filename:  config.log.path + 'app-error.log', 
            timestamp: timeFormat, 
            json: false,            
            maxsize: 5242880, //5MB
            maxFiles: 5
        })
    ],
    exitOnError: false
});

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message)
    }
}
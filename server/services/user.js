let Ngu = Negiup;
let utilities = Ngu.libs.utilities;

exports.authenUser = function (input, cb) {
    Ngu.model('User')
        .findOne({
            $or: [{ 'username': input.username },
            { 'email': input.username },
            { 'phone.number': input.username }],
            'password': input.password,
        },
            function (err, user) {
                if (err || !user) cb(err || new Error('401026001 username or password not match'))
                else if (user) {
                    logger.debug('[LOGIN] ' + user._id + '')
                    cb(null, user);
                }
            })
}
exports.checkUser = function (user, cb) {
    Ngu.model('User')
        .findOne({
            $or: [
                { phone: user.phone },
                { email: user.email },
                { username: user.username }
            ],
        }, function (err, user) {
            if (err) cb(err)
            else if (user) {
                cb(null, user)
            }
            else cb(null, null)
        })
}
exports.getUserByToken = function (token, cb) {
    Ngu.model('Token')
        .findOne({
            token: token,
            status: 'PUBLISHED'
        })
        .populate({ path: 'user' })
        .exec(function (err, token) {
            if (user) cb(null, token.user);
            else cb(err || new Error('[USER SERVICE] cannot verify token'));
        })
}
exports.update = function (data, cb) {
    var input = data;

    checkData = () => {
        errors = []
        if (input.phone.number == null) errors.push({ 'phone': { name: '_2' } })
        if (input.email == null) errors.push({ email: '_2' })
        if (input.birthday == null) errors.push({ birthday: '_2' })
        return errors
    }
    Ngu.model("User").findById(input.id,
        function (err, user) {
            user.set(input)
            user.save()
            if (user) cb(null, user);
            else if(err || !user || checkData().length) cb(err || new Error('404026001 can not update information'));
        }
    );


}
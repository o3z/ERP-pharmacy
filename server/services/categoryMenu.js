Ngu = Negiup

map = []
mCategories = []
function transformToTree(arr) {
    var nodes = {};
    return arr.filter(function (obj) {
        var id = obj["_id"],
            parentId = obj["parent"];

        nodes[id] = _.defaults(obj, nodes[id], {
            children: []
        });
        parentId && (nodes[parentId] = (nodes[parentId] || {
            children: []
        }))["children"].push(obj);

        return !parentId;
    });
}
function load(cb){
        Ngu.model('Category')
        .find({
            status: 'DRAFT'
        })
        .lean()
        .exec(function (err, categories) {
            // console.log(transformToTree(categories))
            mCategories = _.cloneDeep(categories)
            cb(err, categories)
        })
}
function pathObject(arrays){
    return _.flatten(arrays.map(function(value) {
        if(value.children.length > 0) {
            a = pathObject(value.children)
            return a.map((o)=>{
                return (value.slug+'/'+o)
            })
        }
        else return value.slug
    }));
}
load(function(err, categories){
    logger.info('[service] categoryMenu auto create map category')
    menu = transformToTree(categories)
    map = pathObject(menu)
})
module.exports.getMap = function () {
    return map
}
module.exports.getCategory = function () {
    return mCategories
}
module.exports.findParent = function(slug) {
    index = _.findIndex(map, function(o) {  return o.includes(slug) })
    if(index > -1 ) array = map[index].split('/')
    else array = []
    return array.slice(0, array.indexOf(slug))
}
module.exports.findChild = function(slug) {
    array = map.map(function(o) {
        uri = o.includes(slug)
        if(uri) {
            return {
                uri: uri? o : false,
                slug: o.slice(o.indexOf(slug),  o.length).split('/').pop()
            }
        }
        else return false
    })
    _.pull(array, false)
    return array
}
var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var crypto = require('crypto');
/*
  File model
  ==================
  Error code group: 022
  Purpose: xử lí tạo File
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var MessageSchema = new mongoose.Schema({
    owner: { type: Types.ObjectId, ref: 'User' },
    message: { type: String },
    file: [{ type: Types.ObjectId, ref: 'File' }],
    conversation: { type: Types.ObjectId, ref: 'Conversation' },
    status: { type: Types.String, enum: ['DRAFT', 'LOCK', 'PUBLISHED'], default: 'PUBLISHED' },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Message = mongoose.model('Message', MessageSchema);

MessageSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {

});
module.exports = Message;

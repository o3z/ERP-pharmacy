var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;

/*
  Tracking model
  ==================
  Error code group: 009
  Purpose: Bắt truy cập và hành của người dùng
  Note: 
  - status = [DRAFT, PUBLISHED, ERROR]
  - data là định dạng dữ liệu JSON
*/

var TrackingSchema = new mongoose.Schema({
    cookie : { type: String},
    user : { type: Types.ObjectId, ref: 'User', noedit: true},
    referer : { type: String},
    search : { type: String},
    category : { type: Types.ObjectId, ref: 'Category', noedit: true},
    post : { type: Types.ObjectId, ref: 'Post', noedit: true},
    duration : { type: Number },
    rating : { type: Number }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});
 
TrackingSchema.pre('remove', function(next){
next()
})


var Tracking = mongoose.model('Tracking', TrackingSchema);
module.exports = Tracking;

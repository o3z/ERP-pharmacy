var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var validator = require('validator');
var crypto = require('crypto');

/**
*  User model
 * ==================
*  Error code group: 001
*  Purpose: xử lí các thông tin user, phân quyền user, lịch sử giao dịch, lịch sử tham gia hoạt động, comment
*  Note: 
 * - status = [DRAFT, LOCK, UNLOCK ,PUBLISHED]
 * - unit = [vnd]
 * - specialize
 * - gender = [U, M, F]
 * - type: ['PATIENT', 'OWNER', 'SUPERADMIN', 'STAFF']
*/

var UserSchema = new mongoose.Schema({
    name: { type: Types.String, required: true },
    gender: { type: Types.String, enum: ['U', 'M', 'F'], default: 'U' },
    status: { type: Types.String, enum: ['DRAFT', 'LOCK', 'UNLOCK', 'PUBLISHED'], default: 'DRAFT' },
    type:{ type: Types.String, enum: ['PATIENT', 'OWNER', 'SUPERADMIN', 'STAFF', 'TEST'], default: 'PATIENT' },
    role: { type: Types.ObjectId, ref: 'Role' },
    phone: {
        code: { type: Types.String, default: '84'},
        number: { type: Types.String, required: true , unique: true, index: true},
    },
    email: { type: Types.String, unique: true, required: true},
    valid : {
        email : { type: Types.Boolean, default: false},
        phone : { type: Types.Boolean, default: false }
    },
    username: { type: Types.String, unique: true, index: true},
    password: { type: Types.String },
    birthday: { type: Date, default: moment(), format: 'DD/MM/YYYY' },
    wallet: {
        credit: {
            limit: { type: Number, default: 0},
            used: { type: Number, min: 0},
            expried: { type: Types.Date },
        },
        debit: { type: Number, default: 0},
        unit: { type: Types.String, enum: ['vnd'], default: 'vnd' },
    },
    bank: {
        account: { type: Types.String },
        branch : { type: Types.String },
        main: { type: Types.String },
    },
    place: {
        number: { type: Types.String },
        street: { type: Types.String },
        town: { type: Types.String },
        city: { type: Types.String },
        codeCity: { type: Number },
        country: { type: Types.String },
    },
    avatar: { type: Types.String },
    certificate: {
        overview: { type: Types.String },
        images: { type: Types.String },
    },
    specialize: { type: Types.String },
    graduationYear: { type: Types.Date },
    mission: { type: Types.String },
    search: { type: Types.String, index: true},
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

var User = mongoose.model('User', UserSchema);

UserSchema.pre('remove', function(next){
    next(new Error('405001001 cannot remove User'));
})
// TEST: hash password
// .pre('save', function(next){
// 	var self = this
// 	if(self.isModified('password') && self.password){
//         self.password = crypto.pbkdf2Sync(
//             self.password, 
//             process.env.PASSWORD_SALT, 
//             1234, 
//             64, 
//             'sha512').toString('hex');
//         next()
//     } else next()
// })
// TEST: check format email
.pre('save', function(next){
	let self = this
    if((self.isModified('email') ||self.isNew)  && self.email) { 
        if( validator.isEmail(self.email)) next()
        else next(new Error('405001001 can not validate email'))
    } else next()
    
})
// TEST: search
.pre('save', function(next){
	let self = this
    self.birthday = moment(self.birthday, 'DD/MM/YYYY').toISOString()
    next()
})
// TEST: search
.pre('save', function(next){
	let self = this
    self.search = self.name + ', ' + self.username +', '+ self.phone +', '+ self.email
    next()
})

// TEST: send notify
// .post('save', function(next){
//     let self = this
//     logger.silly('')
//     next()  
// })
module.exports = User;

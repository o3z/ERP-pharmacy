var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;
var validator = require('validator');

/*
  Tag model
  ==================
  Error code group: 009
  Purpose: Xử lí các thông báo (gửi email, sms, push FB) đến tài khoản user đã liên kết
  Note: 
  - status = [DRAFT, PUBLISHED, ERROR]
  - data là định dạng dữ liệu JSON
*/

var TagSchema = new mongoose.Schema({
    name: { type: String, unique: true },
    slug: { type: String, index: true, unique: true }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

//TODO: validation JSONString
//TODO: validation email
//TODO: validation phone
TagSchema.pre('save', function(next){
    
})

var Tag = mongoose.model('Tag', TagSchema);
module.exports = Tag;

var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
Ngu = Negiup;
lib = Ngu.libs;

/*
  File model
  ==================
  Error code group: 022
  Purpose: xử lí tạo File
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var CategorySchema = new mongoose.Schema({
    name: { type: String, unique: true},
    meta: {
        title: { type: String },
        description: { type: String },
        image: { type: String },
        ogTitle: { type: String },
        ogDescription: { type: String },
        ogImage: { type: String },
    },
    icon: { type: String},
    link:  { type: String},
    slug: { type: String, index: true, unique: true, index: true },
    parent: { type: Types.ObjectId, ref: 'Category' },
    status: { type: String, enum: ['DRAFT', 'PUBLISHED'], default: 'DRAFT' },
    description: { type: String },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Category = mongoose.model('Category', CategorySchema);

CategorySchema.pre('remove', function(next){
    if(this.status == 'PUBLISHED') next(new Error('cannot remove when status is PUBLISHED'))
    else next()
})
.pre('save', function(next){
    logger.info('[CATEGORY] create slug');
    self = this;
    self.slug = Ngu.libs.utilities.slug(self.name);
    next();
})
module.exports = Category;

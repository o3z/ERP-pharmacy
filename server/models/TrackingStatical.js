var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;

/*
  TrackingStatical model
  ==================
  Error code group: 009
  Purpose: Thống kê và lập bộ dữ liệu tracking
  Note: 
  - status = [DRAFT, PUBLISHED, ERROR]
  - data là định dạng dữ liệu JSON
*/

var TrackingStaticalSchema = new mongoose.Schema({
    cookie : { type: String},
    user : { type: Types.ObjectId, ref: 'User', noedit: true},
    referer : { type: String},
    search : { type: String},
    category : { type: Types.ObjectId, ref: 'Category', noedit: true},
    post : { type: Types.ObjectId, ref: 'Post', noedit: true},
    duration : { type: Number },
    rating : { type: Number }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});
 
TrackingStaticalSchema.pre('remove', function(next){
next()
})


var TrackingStatical = mongoose.model('TrackingStatical', TrackingStaticalSchema);
module.exports = TrackingStatical;

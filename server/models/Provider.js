var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;
/*
  File model
  ==================
  Error code group: 022
  Purpose: xử lí tạo File
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var ProviderSchema = new mongoose.Schema({
    headOffice: { type: Boolean, default: false },
    office: { type: Types.ObjectId, ref: 'Provider'},
    avatar: { type: Types.ObjectId, ref: 'File' },
    post: { type: Types.ObjectId, ref: 'Post' },
    name: {
        short: { type: String },
        full: { type: String}
    },
    type: { type: String, enum: ['DRAFT', 'PUBLISHED', 'BAN'], default: 'PUBLISHED' },
    website: { type: String },
    location: {
        full: { type: String },
        number: { type: String },
        street: { type: String },
        county: { type: String },
        city: { type: String },
        country: { type: String },
        description: { type: String },
    },
    document: {
        overview: { type: String },
        images: [{ type: Types.ObjectId, ref: 'File' }],
    }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Provider = mongoose.model('Provider', ProviderSchema);

ProviderSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {
    next()
});
module.exports = Provider;

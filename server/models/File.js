var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var crypto = require('crypto');
/*
  File model
  ==================
  Error code group: 022
  Purpose: xử lí tạo File
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var FileSchema = new mongoose.Schema({
    // user: { type: Types.ObjectId, ref: 'User', required: true },
    status: { type: Types.String, enum: ['DRAFT', 'LOCK', 'PUBLISHED'], default: 'PUBLISHED' },
    type: { type: String, enum: ['IMAGE', 'VIDEO', 'MUSIC', 'DOCUMENT'], default: 'IMAGE', required: true }, 
    name: { type: String },
    unique: { type: String },
    extension: { type: String },
    description: { type: String },
    url: { type: String },
    folder: { type: String },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Files = mongoose.model('File', FileSchema);

FileSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {

});
module.exports = Files;

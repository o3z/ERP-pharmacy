var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/*
  UserExstension model
  ==================
  Error code group: 022
  Purpose: xử lí tạo UserExstension
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var UserExstensionSchema = new mongoose.Schema({
    user: { type: Types.ObjectId, ref: 'User' },
    attribute: { type: String },
    value: { type: String },
    valueNumber: { type: String },
    type: { type: String },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

UserExstension = mongoose.model('UserExstension', UserExstensionSchema);

UserExstensionSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {

});
module.exports = UserExstension;

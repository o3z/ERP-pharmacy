var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
Ngu = Negiup;

/**
 * Invoice model
 * ==== ==============
 * Error code group: 002
 * Purpose: Xử lý hóa đơn, bao gồm các giao giá trị của hóa đơn, thực hiện các transaction hệ thống. Giá trị và số lượng các mặt hàng có trong hóa đơn.
 *  Note: 
 * - status = [DRAFT, LOCK, UNLOCK ,PUBLISHED]

*/

var InvoiceSchema = new mongoose.Schema({
    patient: { type: Types.ObjectId, ref: 'User' },
    code: { type: String, required: true, index: true, unique: true},
    drugStore : { type: Types.ObjectId, ref: 'DrugStore' },
    type: { type: String, enum: ['ORDER', 'IMPORT', 'EXPORT'], default: 'IMPORT'},
    provider: { type: Types.ObjectId, ref: 'Provider' },
    status: { type: String, enum: ['DRAFT', 'DEBT', 'PAID'], default: 'PAID'},
    time: { type: Date, default: moment() },
    image : [{ type: Types.ObjectId, ref: 'File' }],
    totalAmount:  { type: Number },
    excessCash: { type: Number },
    currency : { type: String, enum: ['vnd'], default: 'vnd'},
    details: [{
        merchandise: { type: Types.ObjectId, ref: 'Merchandise' },
        number: { type: Number, min: 0 },
        amount: { type: Number, min: 0 },
        totalAmount: { type: Number, min: 0 }
    }],
    staff: { type: Types.ObjectId, ref: 'Staff' },
    creater: { type: String },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

let Invoice = mongoose.model('Invoice', InvoiceSchema);

InvoiceSchema.pre('remove', function(next){
    next('[INVOICE] cannot remove');
})
//TEST: CANNOT modified when status == PUBLISHED
.pre('save', function(next){
    this.constructor
    .findOne({ _id: this._id})
    .exec(function(err, original){
        if(original && original.status == 'PUBLISHED') next(err || new Error('444xxx001 cannot modified status is PUBLISHED'))
        else if(err) next(err)
        else next()
    })
})
//TEST: CANNOT change type
.pre('save', function(next){
    if(this.isModified('type')) next('404xxx001 can not change type');
    else next();
})
//TEST: UPDATE merchandise
.pre('save', function(next){
    self = this
    if(self.status != 'DRAFT') {
        merchandises = self.details.map(function(detail){ return detail.merchandise })
        Ngu.model('Merchandise')
        .find({$in : merchandises})
        .exec(function(err, merchandises){
            if(merchandises.length > 0){
                if(self.type == 'IMPORT') {
                    merchandises.forEach(function(merchandise, i){
                        merchandise.stock = merchandise.stock + self.details[i].number
                        merchandise.save()
                    })
                } else if(self.type == 'EXPORT') {
                    merchandises.forEach(function(merchandise, i){
                        merchandise.stock = merchandise.stock - self.details[i].number
                        merchandise.save()
                    })
                } else if(self.type == 'ORDER') {
                    merchandises.forEach(function(merchandise, i){
                        merchandise.stock = merchandise.stock - self.details[i].number
                        merchandise.save()
                    })
                }
            } else next(err || new Error('444xxx001'))
        })
    } else next()
})
//TEST: UPDATE revenue for DrugStore
.pre('save', function(next){
    self = this
    if(self.status == 'DEBT') {
        Ngu.model('DrugStore')
        .findOne({
            _id: self.drugStore
        }).exec(function(err, drugStore){
            if(drugStore){
                if(self.type == 'EXPORT') {
                    drugStore.debit = drugStore.debit + self.totalAmount
                } else next(new Error('404xxx001 not support type invoice'))
                drugStore.save()
            } else next(err || new Error('400xxx001 not found DrugStore'))
        })
    } else if(self.status == 'PAID') {
        Ngu.model('DrugStore')
        .findOne({
            _id: self.drugStore
        }).exec(function(err, drugStore){
            if(drugStore){
                if(self.type == 'IMPORT') {
                    drugStore.cost = drugStore.cost + self.totalAmount
                } else if(self.type == 'EXPORT') {
                    drugStore.revenue = drugStore.revenue + self.totalAmount
                    drugStore.debit = drugStore.debit - self.totalAmount
                } else if(self.type == 'ORDER'){
                    drugStore.cost = drugStore.cost + self.totalAmount
                } else next(new Error('404xxx001 not support type invoice'))
                drugStore.save()
            } else next(err || new Error('400xxx001 not found DrugStore'))
        })
    } else next()
})

//TEST: UPDATE wallet for user and DrugStore
// .pre('save', function(next){
//     self = this
//     if(self.status == 'PUBLISHED') {
//         Ngu.model('Transaction')
//         .create({ 

//         })
//         .exec(function(err, merchandises){

//         })
//     } else next()
// })
module.exports = Invoice;

var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var crypto = require('crypto');
/*
  Staff model
  ==================
  Error code group: 022
  Purpose: xử lí tạo Staff
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var StaffSchema = new mongoose.Schema({
    user: { type: Types.ObjectId, ref: 'User', required: true },
    drugStore: { type: Types.ObjectId, ref: 'DrugStore', required: false },
    code: {
        id: { type: String },
        function: { type: String },
    },
    salary: {
        value: { type: Number },
        unit: { type: String, enum: ['vnd'], default: 'vnd', required: false },
        allowance: { type: Number },
    },
    position: { type: String, enum: ['WITHDRAWAL', 'INCOME', 'TOPUP', 'CORRECT', 'CHARGE', 'TRANSFER'], default: 'CORRECT', required: true },
    rating: {
        value: { type: Number },
    }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Staff = mongoose.model('Staff', StaffSchema);

StaffSchema.pre('save', function (next) {
    next();
})
// .pre('save', function (next) {

// });
module.exports = Staff;

var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;
var validator = require('validator');

/*
  Notification model
  ==================
  Error code group: 009
  Purpose: Xử lí các thông báo (gửi email, sms, push FB) đến tài khoản user đã liên kết
  Note: 
  - status = [DRAFT, PUBLISHED, ERROR]
  - data là định dạng dữ liệu JSON
*/

var NotificationSchema = new mongoose.Schema({
    type: { type: Types.String, required: true},
    useTemplate: { type: Types.Boolean, default: true},
    template: { type: Types.ObjectId, ref: 'Template', },
    status: { type: Types.String, enum: ['DRAFT','CANCELLED', 'ERROR', 'PUBLISHED'], default: 'PUBLISHED' },
    data: { type: Types.String, default: "{}" },
    sendToRegisteredUser: { type: Types.Boolean, default: false },
    user: { type: Types.ObjectId, ref: 'User' },
    email: {
        from: { type: Types.String },
        cc: { type: Types.String },
        bcc: { type: Types.String },
        subject: { type: Types.String },
        html: { type: Types.String },
    },
    sms: {
        countryCode: { type: Types.String, default: '84' },
        phone: { type: Types.String },
        text: { type: Types.String },
    },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

//TODO: validation JSONString
//TODO: validation email
//TODO: validation phone
NotificationSchema.pre('save', function(next){
    self = this
    self.constructor
    .findOne({_id: self._id, status: 'PUBLISHED'})
    .exec(function(err, original){
        if(err || original) next(err || new Error('[Notification] is not modified'))
        else next();
    })
})
.pre('save', function(next){
    var self = this
    if(self.status == 'PUBLISHED' && self.user){
        Ngu.model('User')
        .findOne({_id: self.user})
        .select('email phone')
        .exec(function(err, user){
            if(err || !user) next(err || new Error('[Notification] not found user'))
            else {
                self.user = user
                next()
            }
        })
    } else next()
})
.pre('save', function(next){
    let self = this
    if(self.status == 'PUBLISHED'){
        if(self.useTemplate){
            Ngu.model('Template')
            .findOne({
                _id : self.template,
                type: self.type
            }).exec(function(err, template){
                if(err || !template) logger.err(err || new Error('[NotiFy Service] not found Template'))
                else {
                    switch(self.type){
                        case 'email' : 
                        // Render html, subject
                            let renderData = {
                                from: template.from,
                                cc: template.cc,
                                bcc: template.bcc,
                                subject: self.email.subject,
                                html: self.email.html
                            }
                            self.email = renderData
                            Ngu.services.notify.send('email', self.user.email, renderData)
                            next()
                        break;
                    }
                }
            })
        } else {
            switch(self.type){
                case 'email' : 
                // Render html, subject
                    let renderData = {
                        from: self.email.from,
                        cc: self.email.cc,
                        bcc: self.email.bcc,
                        subject: self.email.subject,
                        html: self.email.html
                    }
                    Ngu.services.notify.send('email', self.user.email, renderData)
                    next()
                break;
            }
        }
    } else next();
})

NotificationSchema.statics.execute = function(slug, to, data, cb){
    to = to.toString();
    Promise.all([
        new Promise(function(resolve, reject){
            //TEST: template found by slug
            Ngu.model('Template')
            .findOne({
                slug: slug,
                status: 'PUBLISHED'
            }, function(err, template){
                if(template) resolve(template._id)
                else resolve('')
            })
        }),
        new Promise(function(resolve, reject){
            // TEST: the 'to' is email, id, phone
            var type = ''
            var filter = {
                status : 'DRAFT',
            }
            if(validator.isEmail(to)) type='email';
            else if(validator.isMobilePhone(to, 'vi-VN')) type='phone.number';
            else if(validator.isMongoId(to)) type='_id';
            if(type){
                filter[type] = to;
                Ngu.model('User')
                .findOne(filter, function(err, user){
                    if(user) resolve(user._id.toString());
                    else resolve('')
                })
            } else reject(new Error('[Notification] fail format address User'));  
        })
    ]).then(function(success){
        logger.debug('[Notification] send mail verify')
        var filter = data
        if(success[0]) filter.template = success[0]
        if(success[1]) filter.user = success[1]
        Ngu.model('Notification')
        .create(filter, function(err, notification){
            if(cb) cb(err, notification);
        });
    }, function(err){
        logger.debug('[Notification] NOT send mail verify')
        logger.error(err);
        if(cb) cb(err);
    })
}
var Notification = mongoose.model('Notification', NotificationSchema);
module.exports = Notification;

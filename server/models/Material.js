var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;
var validator = require('validator');

const UNITPRIMARY = ['BAR','AEROSOL', 'BLISTERS', 'BOTTLE', 'STRIP', 'VIAL', 'KIT', 'CAPSULES', 'TABLETS', 'TUBE', 'SYRINGE', 'INHALER']
const UNITSECONDARY = ['BLISTERS','BOX', 'CARTONS', 'POUCHES']
const UNITTERTIARY = ['BOX','BARREL', 'CONTAINER', 'PROJECTOR']
const STATUS = ['DRAFT', 'PUBLISHED', 'BAN'];  
const DOSAGES = ['SOLIDORAL', 'TABLETS', 'CAPSULES', 'GRANULES', 'ORALPOWDER', 'ORALLIQUID']
/*
  Material model
  ==================
  Error code group: 009
  Purpose: Xử lí các thông báo (gửi email, sms, push FB) đến tài khoản user đã liên kết
  Note: 
  - status = [DRAFT, PUBLISHED, ERROR]
  - data là định dạng dữ liệu JSON
*/

var MaterialSchema = new mongoose.Schema({
    name : {
      short: { type: String },
      long: { type: String },
    },
    type: { type: String, enum: ['DRUG', 'MEDICAL']},
    code: { type: String, unique: true },
    status: { type: Types.String, enum: STATUS, default: 'PUBLISHED'},
    description: { type: String },
    ingredients : [{
        name: { type: String },
        dosage: { type: String }
    }],
    uses: { type: Types.String },
    packing: { 
      primary: { 
        number: Number,
        unit: { type: Types.String, enum: UNITPRIMARY }
      },
      second:{ 
        number: Number,
        unit: { type: Types.String, enum: UNITSECONDARY }
      },
      tertiary:{ 
        number: Number,
        unit: { type: Types.String, enum: UNITTERTIARY }
      }
    },
    dosage: { type: Types.String, enum: DOSAGES },
    route: { type: Types.String },
    precautions: { type: String },
    other: { type: String },
    web: { type: String },
    price: { type: Number },
    currency: { type: String, enum : ['vnd']},
    company: [{ type: Types.ObjectId, ref: 'Company' }],
    category: { type: Types.ObjectId, ref: 'MaterialCategory' }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

//TODO: validation JSONString
//TODO: validation email
//TODO: validation phone
MaterialSchema.pre('save', function(next){
    next()
})

var Material = mongoose.model('Material', MaterialSchema);
module.exports = Material;

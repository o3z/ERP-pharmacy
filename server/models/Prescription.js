var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/**
 * Prescription model
 * ==== ==============
 * Error code group: 002
 * Purpose: Xử lí phân quyền nhóm người dùng.
 *  Note: 
 * - status = [DRAFT, LOCK, UNLOCK ,PUBLISHED]

*/

var PrescriptionSchema = new mongoose.Schema({
    user : { type: Types.ObjectId, ref: 'User' },
    organization: { type: Types.ObjectId, ref: 'Organization' },
    diagnostic: { type: String },
    timeExamination: { type: Date, default: moment() },
    merchandise: [
        {
            id: { type: Types.ObjectId, ref: 'Merchandise' },
            frequency: { type: Number, min: 0 },
            take: { type: Number, min: 0 },
            total: { type: Number, min: 0 },
        }
    ],
    route: { type: String },
    duration: { type: Number },
    note: { type: String },
    doctorName: { type: String },
    file: { type: Types.ObjectId, ref: 'File' },
    drugStore: { type: Types.ObjectId, ref: 'DrugStore' },
    invoice: { type: Types.ObjectId, ref: 'Invoice' },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

let Prescription = mongoose.model('Prescription', PrescriptionSchema);

PrescriptionSchema.pre('remove', function(next){
    next('[Prescription] cannot remove');
})
.pre('save', function(next){
    if(this.status == 'PUBLISHED') next('404xxx001 cannot modify Prescription have status PUBLISHED')
    else next()
})
.pre('save', function(next){
    if(this.isModified('type')) next('404xxx001 can not change type');
    else next();
})
.pre('save', function(next){
    // if(this.status == 'PUBLISHED') {

    // }
    // else next()
})
module.exports = Prescription;

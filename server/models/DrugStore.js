var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/**
 * DrugStore model
 * ==== ==============
 * Error code group: 002
 * Purpose: Xử lí phân quyền nhóm người dùng.
 *  Note: 
 * - status = [DRAFT, LOCK, UNLOCK ,PUBLISHED]

*/

var DrugStoreSchema = new mongoose.Schema({
    name: {
        short: { type: Types.String, trim: true},
        full: { type: Types.String, trim: true },
    },
    owner: { type: Types.ObjectId, ref: 'User'},
    status: { type: Types.String, enum: ['DRAFT', 'LOCK', 'PUBLISHED'], default: 'DRAFT'},
    active: { type: Boolean, default: false},
    avatar: { type: Types.String },
    arceage: { type: Types.Number, min: 10 },
    description: { type: Types.String },
    opendingDay: { type: Date },
    revenue: { type: Types.Number, min: 0},
    cost: { type: Types.Number, min: 0},
    debt: { type: Types.Number, min: 0},
    wallet: {
        credit: {
            limit: { type: Number, default: 0},
            used: { type: Number, min: 0},
            expried: { type: Types.Date },
        },
        debit: { type: Number, default: 0},
        unit: { type: Types.String, enum: ['vnd'], default: 'vnd' },
    },
    currency: { type: Types.String, enum: ['vnd'], default: 'vnd'},
    discount: { type: Types.Number, min: 0.7 },
    phone: { 
        code : {type: Types.String},
        number: {type: Types.String}
     },
    fax: { type: Types.String },
    email: { type: Types.String },
    location: {
        number: { type: Types.String },
        street: { type: Types.String },
        county: { type: Types.String },
        city: { type: Types.String },
        country: { type: Types.String },
    },
    document: {
        overview: { type: Types.String },
        images: [{ type: Types.String }],
    },
    drugs: [{ type: Types.ObjectId, ref: 'Drug' }],
    rating: {
        total: { type: Number, min: 0 },
        value: { type: Types.Number, min: 0, max: 5},
    },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});


DrugStoreSchema.pre('save', function(next){
    next();
})
let DrugStore = mongoose.model('DrugStore', DrugStoreSchema);
module.exports = DrugStore;

var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;
var validator = require('validator');

const UNITPRIMARY = ['BAR','AEROSOL', 'BLISTERS', 'BOTTLE', 'STRIP', 'VIAL', 'KIT', 'CAPSULES', 'TABLETS', 'TUBE', 'SYRINGE', 'INHALER']
const UNITSECONDARY = ['BLISTERS','BOX', 'CARTONS', 'POUCHES']
const UNITTERTIARY = ['BOX','BARREL', 'CONTAINER', 'PROJECTOR']
const STATUS = ['DRAFT', 'PUBLISHED', 'BAN'];  
const DOSAGES = ['SOLIDORAL', 'TABLETS', 'CAPSULES', 'GRANULES', 'ORALPOWDER', 'ORALLIQUID']
/*
  MaterialCategory model
  ==================
  Error code group: 009
  Purpose: Xử lí các thông báo (gửi email, sms, push FB) đến tài khoản user đã liên kết
  Note: 
  - status = [DRAFT, PUBLISHED, ERROR]
  - data là định dạng dữ liệu JSON
*/

var MaterialCategorySchema = new mongoose.Schema({
    name : { type: String }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

//TODO: validation JSONString
//TODO: validation email
//TODO: validation phone
MaterialCategorySchema.pre('save', function(next){
    next()
})

var MaterialCategory = mongoose.model('MaterialCategory', MaterialCategorySchema);
module.exports = MaterialCategory;

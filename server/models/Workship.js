var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/*
  Workship model
  ==================
  Error code group: 022
  Purpose: xử lí tạo Workship
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var WorkshipSchema = new mongoose.Schema({
    name: { type: String },
    block: {
        from: Date,
        to: Date
    },
    drugStore: { type: Types.ObjectId, ref: 'DrugStore', required: true },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Workship = mongoose.model('Workship', WorkshipSchema);

WorkshipSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {

});
module.exports = Workship;

var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/*
  Rule model
  ==================
  Error code group: 003
  Purpose: Xử lí các quyền cấm lên một URL
  Note: 
  - status = [DRAFT, PUBLISHED]
  - regex is check isRegexp
  - method = [GET, POST, PUT, DELETE]
  - option = [ONLYONE, ANY]
  - body : function check body
*/

var RuleSchema = new mongoose.Schema({
    name: { type: Types.String },
    status: { type: Types.String, enum: ['DRAFT', 'PUBLISHED'], default: 'PUBLISHED' },
    path: {
        regexp: { type: Types.String },
        methods: [{ type: Types.String }]
    },
    option : { type: Types.String, enum: ['ANY'], default: 'ANY' },
    body: { type: Types.String }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

let Rule = mongoose.model('Rule', RuleSchema);

RuleSchema.pre('save', function(next){
    next();
})
module.exports = Rule;

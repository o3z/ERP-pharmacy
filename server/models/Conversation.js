var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var crypto = require('crypto');
/*
  File model
  ==================
  Error code group: 022
  Purpose: xử lí tạo File
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var ConversationSchema = new mongoose.Schema({
    staff: { type: Types.ObjectId, ref: 'Staff' },
    patient: { type: Types.ObjectId, ref: 'User' },
    prescription: [{ type: Types.ObjectId, ref: 'Prescription' }],
    drugStore: { type: Types.ObjectId, ref: 'DrugStore' },
    status: { type: Types.String, enum: ['DRAFT', 'LOCK', 'PUBLISHED'], default: 'PUBLISHED' },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Conversation = mongoose.model('Conversation', ConversationSchema);

ConversationSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {

});
module.exports = Conversation;

var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/*
  Schedule model
  ==================
  Error code group: 022
  Purpose: xử lí tạo Schedule
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var ScheduleJobSchema = new mongoose.Schema({
    name: { type: String },
    status: { type: Types.String, enum: ['RUNNING', 'PENDING', 'WAITING'], default: 'RUNNING' },
    recurrence: { type: Boolean },
    date: { type: Date },
    rule: {
        start: { type: Date },
        end: { type: Date },
        second: [{ type: Number }],
        minute: [{ type: Number }],
        hour: [{ type: Number }],
        date: [{ type: Number }],
        month: [{ type: Number }],
        year: [{ type: Number }],
        dayOfWeek: [{ type: Number }],
    },
    function: {
        name: { type: String },
        params: [{ type: String }],
        body: { type: String },
    },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

ScheduleJob = mongoose.model('ScheduleJob', ScheduleJobSchema);

ScheduleJobSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {

});
module.exports = ScheduleJob;

var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var crypto = require('crypto');
/*
  Transaction model
  ==================
  Error code group: 022
  Purpose: Xử lý các giao dịch liên quan đến wallet của user hoặc drugstore
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var TransactionSchema = new mongoose.Schema({
    user: { type: Types.ObjectId, ref: 'User', required: true },
    drugStore: { type: Types.ObjectId, ref: 'DrugStore' },
    status: { type: Types.String, enum: ['DRAFT', 'LOCK', 'PUBLISHED'], default: 'PUBLISHED' },
    expired: { type: Types.Date, default: moment().add(7, 'd') },
    type: { type: String, enum: ['WITHDRAWAL', 'INCOME', 'TOPUP', 'CORRECT', 'CHARGE', 'TRANSFER', 'LOCK', 'UNLOCK'], default: 'CORRECT', required: true },
    sign: { type: String, enum: ['IN', 'OUT'], default: 'IN', required: true },
    amount: { type: Number, min: 0, required: true },
    description: { type: String },
    invoice: { type: Types.ObjectId, ref: 'Invoice' },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Transaction = mongoose.model('Transaction', TransactionSchema);

TransactionSchema.pre('save', function (next) {
    this.constructor
    .findOne({ _id: this._id})
    .exec(function(err, original){
        if(original && original.status == 'PUBLISHED') next(new Error('444xxx001'))
        else if(err) next(err)
        else next()
    })
})
.pre('save', function (next) {
    self = this
    if(self.status == 'PUBLISHED'){


    } else next()
});
module.exports = Transaction;

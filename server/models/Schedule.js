var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var crypto = require('crypto');
/*
  Schedule model
  ==================
  Error code group: 022
  Purpose: xử lí tạo Schedule
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var ScheduleSchema = new mongoose.Schema({
    name: { type: String },
    user: { type: Types.ObjectId, ref: 'User', required: true },
    status: { type: Types.String, enum: ['DRAFT', 'LOCK', 'PUBLISHED'], default: 'PUBLISHED' },
    calendar: {
        time: { type: Types.Date },
        workshift: { type: Types.ObjectId, ref: 'Workship' },
        block: {
            from: { type: Types.Date },
            to: { type: Types.Date },
        }
    }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Schedule = mongoose.model('Schedule', ScheduleSchema);

ScheduleSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {

});
module.exports = Schedule;

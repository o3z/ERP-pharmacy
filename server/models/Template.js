var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/*
  Template model
  ==================
  Error code group: 015
  Purpose: Xử lí template cho email marketing, email transaction, sms, chatbot, push, ...
  Note: 
  - status = [DRAFT, PUBLISHED, ARCHIVED]
  - type = ['EMAIL, SMS, WEB, CHATFUEL, MESSENGER, PUSH']
*/

var TemplateSchema = new mongoose.Schema({
    name: { type: Types.String, required: true, unique: true, index: true },
    drugStore: { type: Types.ObjectId, ref: 'DrugStore' },
    status: { type: Types.String, enum: ['DRAFT', 'PUBLISHED', 'ARCHIVED'], default: 'PUBLISHED' },
    type: { type: Types.String, enum: ['SMS', 'EMAIL']},
    email: {
        from: { type: Types.String },
        subject: { type: Types.String },
        cc: { type: Types.String },
        bcc: { type: Types.String },
        html: { type: Types.String },
    },
    sms: {
        text: { type: Types.String },
    },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

let Template = mongoose.model('Template', TemplateSchema);

TemplateSchema.pre('save', function(next){
    next();
})
module.exports = Template;



var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;

/*
  Post model
  ==================
  Error code group: 009
  Purpose: Xử lí các thông báo (gửi email, sms, push FB) đến tài khoản user đã liên kết
  Note: 
  - status = [DRAFT, PUBLISHED, ERROR]
  - data là định dạng dữ liệu JSON
*/

var PostSchema = new mongoose.Schema({
    name: { type: String, required: true },
    slug: { type: String, unique: true },
    status: { type: String, enum: ['DRAFT', 'PUBLISHED'], default: 'DRAFT' },
    link: { type: String },
    meta: {
        title: { type: String },
        description: { type: String },
        image: { type: String },
        ogTitle: { type: String },
        ogDescription: { type: String },
        ogImage: { type: String },
    },
    author: { type: Types.ObjectId, ref: 'User' },
    approver: { type: Types.ObjectId, ref: 'User' },
    source: { type: String },
    content: {
        overview: { type: String },
        brief: { type: String },
        full: { type: String },
    },
    reference: { type: String },
    rating: { type: Number, default: 0 },
    vote: { type: Number, default: 0 },
    viewer: { type: Number, default: 0 },
    category: { type: Types.ObjectId, ref: 'Category' },
    tag: { type: Types.ObjectId, ref: 'Tag' },
    timeOpen: { type: Types.Date, default: moment() },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});
 
//TODO: validation JSONString
//TODO: validation email
//TODO: validation phone
PostSchema.pre('remove', function(next){
    if(this.status == 'PUBLISHED') next(new Error('cannot remove when status is PUBLISHED'))
    else next()
})
.pre('save', function(next){
    logger.info('[POST] create slug');
    self = this;
    self.slug = Ngu.libs.utilities.slug(self.name);
    next();
})

var Post = mongoose.model('Post', PostSchema);
module.exports = Post;

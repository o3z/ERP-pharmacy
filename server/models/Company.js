var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;
/*
  File model
  ==================
  Error code group: 022
  Purpose: xử lí tạo File
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var CompanySchema = new mongoose.Schema({
    headOffice: { type: Boolean, default: false },
    office: { type: Types.ObjectId, ref: 'Company'},
    avatar: { type: Types.ObjectId, ref: 'File' },
    post: { type: Types.ObjectId, ref: 'Post' },
    name: {
        short: { type: String },
        full: { type: String}
    },
    type: { type: String, enum: ['DRAFT', 'PUBLISHED', 'BAN'], default: 'PUBLISHED' },
    website: { type: String }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Company = mongoose.model('Company', CompanySchema);

CompanySchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {
    next()
});
module.exports = Company;

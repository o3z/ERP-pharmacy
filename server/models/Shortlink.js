var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/*
  Schedule model
  ==================
  Error code group: 022
  Purpose: xử lí tạo Schedule
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var ShortlinkSchema = new mongoose.Schema({
    short: { type: String },
    token: { type: Types.ObjectId, ref: 'Token' },
    expried: { type: Date },
    long: { type: String }
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Shortlink = mongoose.model('Shortlink', ShortlinkSchema);

ShortlinkSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {

});
module.exports = Shortlink;

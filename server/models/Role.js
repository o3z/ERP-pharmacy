var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;

/**
 * Role model
 * ==================
 * Error code group: 002
 * Purpose: Xử lí phân quyền nhóm người dùng.
 *  Note: 
 * - status = [DRAFT, LOCK, UNLOCK ,PUBLISHED]

*/

var RoleSchema = new mongoose.Schema({
    name: { type: Types.String, required: true, unique: true, index: true },
    status: { type: Types.String, enum: ['DRAFT', 'PUBLISHED'], default: 'DRAFT' },
    rule: [{ type: Types.ObjectId, ref: 'Rule', required: true }],
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

let Role = mongoose.model('Role', RoleSchema);

RoleSchema.pre('save', function(next){
    next();
})
module.exports = Role;

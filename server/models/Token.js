var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var crypto = require('crypto');
/*
  Token model
  ==================
  Error code group: 022
  Purpose: xử lí tạo token
  Note: 
  - status = [DRAF, PUBLISHED]
*/

var TokenSchema = new mongoose.Schema({
    user: { type: Types.ObjectId, ref: 'User', noedit: true},
    token  : { type: Types.String, unique: true, index: true},
    status  : { type: Types.String, enum: ['DRAFT', 'LOCK', 'PUBLISHED'], default: 'PUBLISHED' },
    expired  : { type: Types.Date, default: moment().add(7,'d'), noedit: true},
    type: { type: String, enum: ['TOKEN', 'OTP'], default: 'TOKEN', required: true },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

Token = mongoose.model('Token', TokenSchema);

TokenSchema.pre('save', function (next) {
    next();
})
.pre('save', function (next) {
    var self = this;
    if(self.isNew){
        self.token = crypto.pbkdf2Sync(
            self._id.toString(), 
            process.env.PASSWORD_SALT, 
            5000, 
            64, 
            'sha512').toString('hex');
        next();
    } else next();
});
module.exports = Token;

var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
var Ngu = Negiup;
var validator = require('validator');

/*
  Organization model
  ==================
  Error code group: 009
  Purpose: Xử lí các thông báo (gửi email, sms, push FB) đến tài khoản user đã liên kết
  Note: 
  - status = [DRAFT, PUBLISHED, ERROR]
  - data là định dạng dữ liệu JSON
*/

var OrganizationSchema = new mongoose.Schema({
    name: {
        full: { type: String },
        short: { type: String },
    },
    status: { type: Types.String, enum: ['DRAFT', 'PUBLISHED'], default: 'PUBLISHED' },
    avatar: { type: String },
    founded: { type: Number },
    type: { type: Types.String, enum: ['DRAFT', 'PUBLISHED'], default: 'PUBLISHED' },
    website: { type: String },
    phone: { type: String },
    fax: { type: String },
    email: { type: String },
    location: {
        number: { type: String },
        street: { type: String },
        county: { type: String },
        city: { type: String },
        country: { type: String },
        description: { type: String },
    },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

//TODO: validation JSONString
//TODO: validation email
//TODO: validation phone
OrganizationSchema.pre('save', function(next){
    
})

var Organization = mongoose.model('Organization', OrganizationSchema);
module.exports = Organization;

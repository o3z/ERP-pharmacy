var mongoose = require('mongoose');
var Types = mongoose.Schema.Types;
Ngu = Negiup
/**
 * Merchandise model
 * ==== ==============
 * Error code group: 002
 * Purpose: Xử lí phân quyền nhóm người dùng.
 *  Note: 
 * - status = [DRAFT, LOCK, UNLOCK ,PUBLISHED]

*/

var MerchandiseSchema = new mongoose.Schema({
    material: { type: Types.ObjectId, ref: 'Material' },
    price: {
        in: { type: Number },
        out: { type: Number },
    },
    code: { type: Number, default: 0 },
    exp: { type: Date },
    mfg: { type: Date },
    lot: { type: String },
    req: { type: String },
    stock: { type: Number },
    min: { type: Number },
    drugStore: { type: Types.ObjectId, ref: 'DrugStore' },
    search: { type: String, index: true },
}, {timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' }});

MerchandiseSchema.index({ material: 1, code: 1, drugStore: 1},{unique: true})

let Merchandise = mongoose.model('Merchandise', MerchandiseSchema);

MerchandiseSchema.pre('save', function(next){
   if(this.stock < 0) next(new Error('400xxx001'))
   else next()
})
.pre('save', function(next){
    let self = this
    Ngu.model('Material')
    .findById(self.material)
    .lean()
    .exec(function(err, material){
        if(err || !material) next(new Error('400xxx001'))
        else {
            self.search = Ngu.libs.utilities.slug2(material.name.long)
            next()
        }
    })
 })
module.exports = Merchandise;

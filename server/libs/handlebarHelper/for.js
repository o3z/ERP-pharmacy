var Handlebars = require('handlebars');
/* Handlebars helper fix format time on Template*/
Handlebars.registerHelper('mfor', function(from, to, incr, block) {
	var accum = '';
    for(var i = from; i < to; i += incr)
        accum += block.fn(i);
    return accum;
});
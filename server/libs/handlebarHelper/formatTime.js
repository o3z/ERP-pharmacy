var Handlebars = require('handlebars');
/* Handlebars helper fix format time on Template*/
Handlebars.registerHelper("formatTimeOption", function(value, formatText) {
	// let format = Handlebars.escapeExpression(formatText);
	let dateTime = moment(value).format(formatText);
	// logger.silly('testd' + dateTime)
	return dateTime;
});
module.exports = function(source, condition){
    let temp = {}
    let result = Object.keys(condition).map(function(key){
        if(JSON.stringify(source[key]) == JSON.stringify(condition[key])) return key
    })
    return result;
}
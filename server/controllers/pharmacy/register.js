var Ngu = Negiup;
let authenUser = Ngu.services.user.authenUser;
let getToken = Ngu.services.token.getToken;
let checkUser = Ngu.services.user.checkUser;
let getUserByToken = Ngu.services.user.getUserByToken;

/**
 * TEST: login by username or email or phoneNumber
 */
exports.step1 = function (req, res, next) {
    var body = req.body;
    input = {
        phone: {
            code: body.phone.code,
            number: body.phone.number
        },
        email: body.email,
        username: body.username,
    }
    checkData = (data)=>{
        errors = []
        if (input.phone.number == data.phone.number) errors.push({'phone' : {name: '_2'}})
        if (input.email == data.email) errors.push({email : '_2'})
        if (input.username == data.username) errors.push({username : '_2'})
        return errors
    }
    checkUser(input, function (err, user) {
        if (user) { 
            errorCode = checkData(user)
            res.success(errorCode)
        }
        else if (err) { res.err(err || new Error('can not verify info user register')) }
        else { res.success({ step: 'step2' }) }
    })
}
exports.step2 = function (req, res, next) {
    logger.debug('[PHAMARCY CONTROLLER] step2 start')
    var body = req.body;
    token = req.headers.token || req.headers.authorization;
    input = {
        name: body.name,
        gender: body.gender,
        phone: {
            code: body.phone.code,
            number: body.phone.number
        },
        email: body.email,
        username: body.username,
        password: body.password,
        birthday: body.birthday,
    }

    async.waterfall([
        function (cb) {
            if(token){
                logger.debug('[PHAMARCY CONTROLLER] 1.1 login token')
                getUserByToken(token, function(err, user){
                    if (user) {
                        logger.debug('[PHAMARCY CONTROLLER] 1.1.1 user exsited')
                        cb(null, user) 
                    }
                    else {
                        logger.debug('[PHAMARCY CONTROLLER] 1.1.2 create user')
                        Ngu.model('User')
                        .create([input], function (err, users) {
                            if (users.length > 0) { cb(null, users[0]) }
                            else { cb(err || new Error('[PHARMACY CONTROLLER] cannot create User')) }
                        })
                    }
                })
            } else {
                logger.debug('[PHAMARCY CONTROLLER] 1.2 login account')
                authenUser( input, function (err, user) {
                    if (user) { 
                        logger.debug('[PHAMARCY CONTROLLER] 1.2.1  user exsited')
                        cb(null, user) 
                    }
                    else {
                        logger.debug('[PHAMARCY CONTROLLER] 1.2.2 create user')
                        Ngu.model('User')
                        .create([input], function (err, users) {
                            if (users.length > 0) { cb(null, users[0]) }
                            else { cb(err || new Error('[PHARMACY CONTROLLER] cannot create User')) }
                        })
                    }
                })
            }
        }, function (user, cb) {
            drugStore = {
                name: {
                    full: body.drugStore.name
                },
                owner: user._id
            }
            logger.debug('[PHAMARCY CONTROLLER] create drugstore')
            Ngu.model('DrugStore')
            .create([drugStore], function (err, drugStores) {
                if (drugStores.length > 0) {
                    cb(null, drugStores[0])
                } else {
                    logger.debug('[PHAMARCY CONTROLLER] cannot create DrugStore')
                    cb(err || new Error('[PHARMACY CONTROLLER] cannot create DrugStore'))
                }
            })
        }
    ], function (err, result) {
        if(err) { res.err(err) }
        else {
            logger.silly('[PHARMACY CONTROLLER] new user ' + result.owner)
            Ngu.model('Token')
            .create([{
                user: result.owner,
                type: 'OTP'
            }], function (err, tokens) {
                if (tokens.length > 0) {
                    let data = {
                        type: 'email',
                        useTemplate: false,
                        email: {
                            from: 'hungnhuan1996@gmail.com',
                            cc: 'tungnv211996@gmail.com',
                            subject: 'hello',
                            html: '<p>To complete the email verification process, click the following link and then log in to your account:</p> <br>' + config.host.pharmacy + config.path.verify + tokens[0].token,
                        }
                    }
                    Ngu.model('Notification').execute('', result.owner, data);
                    res.success(result);
                } else res.err(err || new Error('not opt available'));
            })
        }
    })
}
exports.step3 = function (req, res, next) {
    var body = req.body;
    otp = req.query.otp;
    authenUser(body, function(err, user){
        if(user) {
            Ngu.model('Token')
            .findOne({
                user: user._id,
                type: 'OTP',
                token: otp,
                status:'PUBLISHED'
            })
            .exec(function(err, token){
                if(token) {
                    user.set('status','PUBLISHED');
                    user.save();
                    Ngu.model('DrugStore')
                    .find({ 
                        owner: user._id,
                        status: 'DRAFT'
                    },function(err, drugStores){
                        drugStores.forEach(drugStore => {
                            drugStore.set('status','PUBLISHED');
                            drugStore.save();
                        });
                    })
                    getToken(user._id, function(err, token){
                        if(token) res.success(token.token);
                        else res.err(err);
                    });
                } else res.err(err || new Error('otp not match : ' + otp));
            });
        } if(err) res.err(err);
    });
}
Ngu = Negiup
var responseData = {
    userInfo: {},
    staffInfo: {}
};
module.exports = function (req, res, next) {
    Ngu.model("User")
        .findOne({ _id: req.params.id })
        .exec(function (err, user) {
            if (user) {
                responseData.userInfo = user
                Ngu.model("Staff")
                    .findOne({ 'user': req.params.id })
                    .exec(function (err, staff) {
                        if (staff) {
                            responseData.staffInfo = staff
                        }
                    });
                res.success(responseData);
            } else res.err(err || new Error('404xxx001 Data not found'))
        });

}
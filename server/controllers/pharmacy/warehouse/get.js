Ngu = Negiup

const MODELS = ['Merchchandise', 'Company', 'Material'];

let filterModel = function (model) {
    return MODELS[_.indexOf(MODELS, model)];
}
/**
 * WAREHOUSE CONTROLLER GET FUNCTION
 * @param {params.id, params.model, query.fields, query.skip, query.limit, query.sort, query.distinct} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports = function (req, res, next) {
    data = {
        model: req.params.model,
        filter: req.query.filter || {},
        fields: req.query.fields || '',
        skip: Number.parseInt(req.query.skip) || 0,
        limit: Number.parseInt(req.query.limit) || 10,
        sort: req.query.sort,
        distinct: req.query.distinct,
        count: req.query.count
    }
    if (req.params.id) data.filter['_id'] = req.params.id

    Ngu.model(filterModel(_.startCase(data.model)).replace(' ', ''))
        .find(data.filter)
        .select(data.fields)
        .skip(data.skip)
        .limit(data.limit)
        .sort(data.sort)
        .exec(function (err, results) {
            if(err || !results) res.err(err || new Error('404xxx001 Data not found'))
            else if(data.limit == 1) {
                res.success(results[0]);
            } else {
                var result = data.distinct ? distinct(results) : results;
                result = data.count ? result.length() : result;
                res.success(result);
            }
        });
}
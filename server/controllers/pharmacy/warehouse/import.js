/**
 * @author [Ivan]
 * @email [ivannguyen.it@gmail.com]
 * @create date 2018-04-20 11:33:56
 * @modify date 2018-04-20 11:33:56
 * @desc [description]
*/
Ngu = Negiup

/**
 * WAREHOUSE CONTROLLER IMPORT FUNCTION
 * 
 * @param {*} req.body
 * @param {*} res 
 * @param {*} next 
 * 
 * step?
 * step1: create or update merchandise
 * step2: create invoice
 */

module.exports = function(req, res, next){
    invoice = req.body.invoice
    merchandises = req.body.merchandises
    infos = merchandises.map(function(merchandise){
        return { material: merchandise.material,
                 lot : merchandise.lot }
    })
   async.waterfall([
       function(cb){
        Ngu.model('Merchandise')
        .find({ $in: infos})
        .exec(function(err, merchans){
            if(merchans.length > 0) {
                details = infos.map(function (info, i) {
                    index = _.findIndex(merchans, info)
                    if (index < 0) {
                        Ngu.model('Merchandise')
                        .create(merchandises[i])
                        .exec(function(err, datas){
                            if(datas.length > 0) {
                                return {
                                    merchandise: datas[0]._id,
                                    number: merchandises[i].number,
                                    amount: merchandises[i].amount,
                                    totalAmount: merchandises[i].totalAmount
                                }
                            }
                            else cb(err || new Error('404xxx001 Data not found'))
                        }); 
                    } else {
                        return {
                            merchandise: merchans[index]._id,
                            number: merchandises[i].number,
                            amount: merchandises[i].amount,
                            totalAmount: merchandises[i].totalAmount
                        }
                    }
                })
                
                cb(null, details)
            }
            else cb(err || new Error('404xxx001 Data not found'))
        })
    }, function(details, cb){
        invoice.details = details
        Ngu.model('Invoice')
        .create(invoice)
        .exec(function(err, invoices){
            if(invoices.length > 0) cb(null, invoices[0])
            else cb(err || new Error('404xxx001 Data not found'))
        });
       }
   ], function(err, result){
       if(err) res.err(err)
       else res.success('Create success')
   })
}
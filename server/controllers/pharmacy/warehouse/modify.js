Ngu = Negiup;

/**
 * WAREHOUSE CONTROLLER IMPORT FUNCTION
 * 
 * @param { invoice} req.body
 * @param {*} res 
 * @param {*} next 
 * 
 * step?
 * step1: create Invoice
 * step2: create record JournalEntry
 * step3: update merchandise
 */
module.exports = function(req, res, next){
   async.waterfall([
       function(cb){
        Ngu.model('Invoice')
        .create(req.body.invoice)
        .exec(function(err, invoices){
            if(invoices.length > 0) cb(null, invoices[0])
            else cb(err || new Error('404xxx001 Data not found'))
        });
       },
       function(invoice, cb){
        journals = req.body.journals.map(function(journal){
            journal.invoice = invoice
            return journal
           })
        Ngu.model('journalWarehouse')
        .create(journals)
        .exec(function(err, journals){
            if(journals.length > 0) cb(null, journals)
            else cb(err || new Error('404xxx001 Data not found'))
        });
       }
   ], function(err, result){

   })
}
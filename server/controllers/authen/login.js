Ngu = Negiup;
let authenUser = Ngu.services.user.authenUser;
let getToken = Ngu.services.token.getToken;

/**
 * Authen Controller
 * ==========
 * Error code group: 026
 * Purpose: 
 */

/**
 * TEST: login by username or email or phoneNumber
 */

 //=======Login ==================================================
module.exports = function(req, res, next){
    var body = req.body
    authenUser(body, function(err, user){
        if(user && user.status == 'PUBLISHED') {
            getToken(user._id, function(err, token){
                if(token) res.success(token);
                else res.err(err);
            });
        } else if(user && user.status == 'DRAFT') {
            res.err( new Error('account miss verify'));
        } else if(err) res.err(err);
    });
}
let Ngu = Negiup;

//======================reset password by email or otp phone=========================
/** 
 * Examples: 
 * Yêu cầu params : { step = [step1, step2, step3]}
 * - Step1 : body = {username}
 * - Step2 : body = {username , phone, email}
 * - Step3 : body = {otp, username, password}
*/
var reset = {
    step1 : function(input, cb){
        Ngu.model('User')
        .findOne({
            'account.username' : input.username,
        },
        function(err, user){
            if(err || !user) cb(new Error('404026001 user not found'))
            else if(user) {
                var responeData = {
                    email : user.email.substring(3, user.email.length) + '***********',
                    phone : '*********' + user.phone.substring(user.phone.length - 2, user.phone.length)
                }
                cb(null, responeData)
            }
        })
    },
    step2 : function(input, cb){
        Ngu.model('User')
        .findOne({
            'account.username' : input.username,
            $or: [(input.phone) ? { 'phone': input.phone } : { 'email': input.email }]
        },
        function(err, user){
            if(err || !user) cb(new Error('404026001 authencation fail'))
            else if(user) {
                //TODO send email or phone authen code
                cb(null, 'Mã xác thực đã gửi đến email và phone của bạn')
            }
        })
    },
    step3 : function(input, cb){
        Ngu.model('Token')
        .findOne({
            'token' : input.otp,
            status : 'PUBLISHED'
        },
        function(err, token){
            if(err || !token) cb(new Error('404026001 otp not match'))
            else if(token) {
                Ngu.model('User')
                .findOneAndUpdate({
                    'account.username' : input.username,
                },
                { 'account.password' : input.password},
                function(err, user){
                    if(err || !user) cb(new Error('404026001 can not update new password'))
                    else if(user) {
                        //TODO send email or phone authen code
                        cb(null, 'update password success')
                    }
                })
            }
        })
    }
}
module.exports = function (req, res, next){
    var body = req.body
    reset[req.params.step](body)
}
var Ngu = Negiup;

/**
 * Generic Controller
 * ==========
 * Error code group: 025
 * Purpose: 
 */

var distinct = function(data){
    return Array.from(new Set(data));
}

// ======================================//
//============Defind API ZONE=============//

/**
 * @api {GET} /:model?fields=createdAt,content&pageIndex=10&pageSize=0&sort=-createdAt get item
 * @apiVersion 1.0.0
 * @apiGroup general
 * @apiName get item
 * @apiDescription get item. To select field, pass comma separated strign into 'fields' query. To sort desc, use negative. Support navigation
 * @apiPermission authenticated
 *
 * 
 * @apiSampleRequest /:model?filters={}&&fields=createdAt,content&limit=10&skip=0&sort=-createdAt&populate=user post&distinct=true&count=true
 */	
exports.get = function(req, res, next){
    Ngu.model(_.startCase(req.params.model).replace(' ',''))
    .find()
	.populate(_.isNil(req.query.populate) ? '' : req.query.populate)
	.select(req.query.fields)
	.skip(Number.parseInt(req.query.skip))
	.limit(Number.parseInt(req.query.limit))
    .sort(req.query.sort)
	.exec(function(err, results){
        if(err) res.err(err || new Error('404xxx001 Data not found'))
        else if(req.query.limit == 1){
            res.success(results[0]);
        }
        else {
            var result =  req.query.distinct? distinct(results) : results;
            result = req.query.count? result.length() : result;
            res.success(result);
        } 
	});
}

/**
 * @api {POST} /:model?fields=createdAt,content&pageIndex=10&pageSize=0&sort=-createdAt get item
 * @apiVersion 1.0.0
 * @apiGroup general
 * @apiName get item
 * @apiDescription get item. To select field, pass comma separated strign into 'fields' query. To sort desc, use negative. Support navigation
 * @apiPermission authenticated
 *
 * 
 * @apiSampleRequest /:model?fields=createdAt,content&limit=10&skip=0&sort=-createdAt&populate=user post&distinct=true&count=true
 */
var create = function(model, input, cb){
    let array = _.isArray(input) ? input : [input]
    Ngu.model(model)
        .create(array,
            function(error, results) {
                if (results.length > 0) {
                    cb(null, results)
                }
                else {
                    cb(error || new Error('404xxx001 cannot create item'))
                }
            })
}
exports.post = function(req, res, next){
    var body = req.body
    logger.debug('[genegic POST]')
    create(_.startCase(req.params.model).replace(' ', ''), body, function (error, result) {
        if(error) res.err(error)
        else res.success(result)
    })
}

/**
 * @api {PUT} /:model?fields=createdAt,content&pageIndex=10&pageSize=0&sort=-createdAt get item
 * @apiVersion 1.0.0
 * @apiGroup general
 * @apiName get item
 * @apiDescription get item. To select field, pass comma separated strign into 'fields' query. To sort desc, use negative. Support navigation
 * @apiPermission authenticated
 *
 * 
 * @apiSampleRequest /:model?fields=createdAt,content&limit=10&skip=0&sort=-createdAt&populate=user post&distinct=true&count=true
 */
var updateAndCreate = function(model, filters, update, options, cb){
    options.new = true;
    options.upsert = true
    Ngu.model(model)
    .update(
        filters,
        update,
        options,
        function(error, result){
            if(result) cb(null, result)
            else cb(error || new Error('404xxx001 cannot update item'))
    })
}
var findAndUpdate = function(model, filters, update, options, cb){
    Ngu.model(model)
    .update(
        filters,
        update,
        options,
        function(error, results){
            if(results) cb(null, results)
            else cb(error || new Error('404xxx001 cannot update item'))
    })
}

exports.put = function(req, res, next){
    var body = req.body;
    switch (body.method){
        case 'updateAndCreate':
            updateAndCreate(_.startCase(req.params.model).replace(' ', ''), 
            {$or: [{_id: res.params.id},
                {"phone.number": res.params.id},
                {"username": res.params.id}]
            }, 
            body.update, 
            body.options, 
            function (error, result) {
                if (error) res.err(error)
                else res.success(result)
            })
            break;
        case 'findAndUpdate':
                findAndUpdate(_.startCase(req.params.model).replace(' ', ''),
                 body.filters, 
                 body.update, 
                 body.options, 
                 function (error, result) {
                    if (error) res.err(error)
                    else res.success(result)
                })
            break;
            default: res.err(new Error('method not supported'));
    }
}

//TEST: Filter data respone
let authenFilter = function(filters, arrays){
    arrays = arrays.map((array)=>{
        filters.forEach((filter)=>{
            delete array[filter];
        });
        return array;
    })
}
/**
 * @api {GET} /search/:model?keyword=xxxxxx&fields=xxx,yyyy,zzzzz&limit=10&skip=0&count=true
 * @apiVersion 1.0.0
 * @apiGroup general
 * @apiName search item
 * @apiDescription search item. To select model and keyword search.  pass comma separated strign into 'fields' query. Support paggin
 * @apiPermission published
 * 
 * @apiSampleRequest /search/user?fields=name,phone.number&limit=10&skip=0&count=true
 */
exports.search = function(req, res, next){
    Ngu.model(_.startCase(req.params.model).replace(' ',''))
    .find({search: { '$regex' : req.query.keyword, '$options' : 'i' }})
	.select(req.query.fields)
	.skip(req.query.skip)
	.limit(req.query.limit)
	.exec(function(err, results){
        if(results && results.length > 0 && req.query.limit == 1){
            res.success(authenFilter(results));
        }
        else if(results.length > 0) {
            var result =  req.query.distinct? distinct(results) : results;
            result = req.query.count? result.length() : authenFilter(result);
            res.success(result);
        } else res.err(err || new Error('404xxx001 Data not found'))
	});
}



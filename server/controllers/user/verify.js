var Ngu = Negiup;
let authenUser = Ngu.services.user.authenUser;
let getToken = Ngu.services.token.getToken;

/**
 * Controller Verify
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports = function (req, res, next){
    var body = req.body;
    otp = req.query.otp;
    authenUser(body, function(err, user){
        if(user) {
            Ngu.model('Token')
            .findOne({
                user: user._id,
                type: 'OTP',
                token: otp,
                status:'PUBLISHED'
            })
            .exec(function(err, token){
                if(token) {
                    user.set('status','PUBLISHED');
                    user.save();
                    getToken(user._id, function(err, token){
                        if(token) res.success(token.token);
                        else res.err(err);
                    });
                } else res.err(err || new Error('otp not match : ' + otp));
            });
        } if(err) res.err(err);
    });
}
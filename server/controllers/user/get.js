Ngu = Negiup

let filterModel = function (model) {
    const models = ['user'];
    return models[_.indexOf(models, model)];
}
/**
 * USER CONTROLLER GET FUNCTION
 * @param {params.id, params.model, query.fields, query.skip, query.limit, query.sort, query.distinct} req 
 * @param {*} res 
 * @param {*} next 
 */
module.exports = function (req, res, next) {
    filter = {}
    console.log(_.startCase(filterModel(req.params.model)))

    if (req.params.id) filter = { _id: req.params.id }
    Ngu.model(_.startCase(filterModel(req.params.model)).replace(' ', ''))
        .find(filter)
        .select(req.query.fields)
        .skip(req.query.skip)
        .limit(req.query.limit)
        .sort(req.query.sort)
        .exec(function (err, results) {
            if (results.length > 0 && req.query.limit == 1) {
                res.success(results[0]);
            }
            else if (results.length > 0) {
                var result = req.query.distinct ? distinct(results) : results;
                result = req.query.count ? result.length() : result;
                res.success(result);
            } else res.err(err || new Error('404xxx001 Data not found'))
        });
}
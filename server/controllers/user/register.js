var Ngu = Negiup;

module.exports = function (req, res, next) {
    var body = req.body;
    input = {
        name: body.name,
        gender: body.gender,
        phone: {
            code: body.phone.code,
            number: body.phone.number
        },
        email: body.email,
        username: body.username,
        password: body.password,
        birthday: body.birthday,
        repassword: body.repassword
    }
    //TODO: validate user null
    if(input.password != input.repassword) res.err(new Error('Password is not equal Reinput password'))
    else if (!body.checkTerm) res.err(new Error('You are not agree with us license yet '))
    else {
        Ngu.model('User')
        .create([input], function (err, users) {
            if(users.length > 0) {
                logger.silly('[USER CONTROLLER] new user' + users[0]._id)
                Ngu.model('Token')
                    .create([{
                        user: users[0]._id,
                        type: 'OTP'
                    }], function (err, tokens) {
                        if (tokens.length > 0) {
                            let data = {
                                type: 'email',
                                useTemplate: false,
                                email:{
                                    from: 'hungnhuan1996@gmail.com',
                                    cc: 'tungnv211996@gmail.com',
                                    subject: 'hello',
                                    html: '<p>To complete the email verification process, click the following link and then log in to your account:</p> <br>' + config.host.customer + config.path.verify + tokens[0].token,
                                }
                            }
                            Ngu.model('Notification')
                            .execute('', users[0]._id, data);
                            res.success(users);
                        } else res.err(err || new Error('not opt available'));
                    })
            } else res.err(err || new Error('cannot create User'));
        }
    )}
}

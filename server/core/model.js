const Importer = require('./importer');
const _Importer = Importer(__dirname);

/**}
 * Ngu library
 * ==================
 *  Error code group: 023
 *  Purpose: Thư viện chứa các xử lí như auto require model, authen high level, ...
 *  Note:
 *####Examples:
 *   - How to use models:
 *          XXX.model('......model something.....');
 *   - How to use auto require all file in folder:
 *          var _importer = Model.importer(__dirname);
 *          var importer = _importer('./....something path.....');
 */
models = {};

var mModel = function(){
    return this;
}

mModel.prototype.init = ()=>{
    models = _Importer('../models');
    mModel.prototype.libs = _Importer('../libs');
    mModel.prototype.services = _Importer('../services'); 
}
mModel.prototype.importer = function(path){
    // console.log(path)
    let imp = require('./importer')
    return imp(path)
};

mModel.prototype.model = (list)=>{
    const m = models[list];
    return m;
};

// mModel.libs = mModel.lib;
// mModel.services =  mModel.service;
module.exports = mModel;
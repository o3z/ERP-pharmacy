var _ = require('lodash');
var Ngu = global.Negiup;
var handlebar = require('handlebars');

/**
 *  Middleware
 * ==================
 *  Error code group: 027
 * 
 */
exports.apiResponse = function(req, res, next) {
    res.notfound = function(){
		let response = {
			code : 404,
			status : 'item not found',
			success : false
		}	
		logger.error('api 404: ', '[REQUEST]' + req.url + ' body:' + JSON.stringify(req.body) + '[RESPONE]' + JSON.stringify(response))
		res.status(404).send(response)
    }
    res.err = function(input) {
		let results = {...input}
		delete results.code;
		delete results.status;
		let httpCode = _.parseInt(_.trim(input.message,' ').substring(0, 3)) || 400;
		let code = (input.message && httpCode) ? parseInt(_.split(input.message, ' ', 1)) : 400
		let status = input.message || input.status || 'bad request';
		if(input.error && input.error.code) code = input.error.code;
		if(input.code) code = input.code;
		let response = {
			code : code,
			status : status,
			message : errorCode[code] ? errorCode[code][locale] : status,
			success : false,
			results : results
		}
		logger.error('api error: ', '[REQUEST]' + req.url + ' body:' + JSON.stringify(req.body) + '[RESPONE]' + JSON.stringify(response))
		res.status(httpCode).send(response)
	}

	res.success = function(results){
		let fields = req.query.fields + ',_id'
		let output = {}
		output = results
		// if((results instanceof Array) && (typeof results[0] == 'object') ) {
		// 	output = []
		// 	results.forEach((result, i, array) => {
		// 		output[i] = {}
		// 		fields.split(',').forEach((field)=> {
		// 			output[i][field] = results[i][field] 
		// 		})				
		// 	})
		// } else if (typeof results == 'number'){
		// 	output = results
		// } else {
		// 	fields.split(',').forEach((field)=> {
		// 		output[field] = results[field] 
		// 	})
		// }
		let response = {
			code : 200,
			status : "success",
			success : true,
			results : output
		}
		logger.debug('api success: ', '[REQUEST]' + req.url + ' body:' + JSON.stringify(req.body) + '[RESPONE]' + JSON.stringify(response))
		res.status(200).send(response)
	}
	next()
}

exports.authentication = function(req, res, next){
	async.waterfall([
		function getRole4Token(cb){
			Ngu.model('Token')
			.findOne({
				token: req.header.token || req.header.authentication || req.header.authen || req.header.Authorization,
				status: 'PUBLISHED'
			})
			.populate({
				path : 'user',
				populate: { path: 'groupRole' }
			})
			.select('user.groupRole.role')
			.exec(function(err, token){
				if(token) cb(err, token.user.groupRole.role)
				else cb(err || new Error('403027001 permission denied'), false)
			})
		},
		function getRole(role, cb) {
			Ngu.model('Role')
				.find({ _id: { $in: role } },
					function (err, roles) {
						if(err || !(roles.length > 0)) cb(err || new Error('403027002 permission denied'), false)
						else if(roles.length > 0) cb(null, roles)
					})
		},
	], function(err, result){
		if(err) res.err(err)
		else {
			var isAuthen = true
			result.map(function(e){
				var _regexp = handlebar.compile(e.path.regexp)
				var fullRegexp = _regexp()
				var regexp = new RegExp(e.path.regexp)
				if(regexp.test(req.path) && (_.indexOf(e.path.methods, req.method) > -1)) isAuthen = false
			})
			if(isAuthen) next()
			else next(new Error('403027003 permission denied'))
		}
	})
}
var express = require('express')
var app = express.Router()
var middleware = require('./middleware');
var bodyParser = require('body-parser');
var Ngu = Negiup;
var importer = Ngu.importer(__dirname);
var controllers = importer('./../controllers/admin');
var authen = importer('./../controllers/authen');

// ************ MIDDLEWARE *****************//
app.all('/*',
      middleware.apiResponse // standardize api response
    //   middleware.processRequest // process request queries 
);

// ************ API PUBLIC ZONE ******************// 
// app.route('/socket')
//       .post(controllers.api.socket.create)

// app.route('/login')
//       .post(authen.login);
app.route('/search/:model')
      .get(controllers.generic.search);

//************ SECURITY LAYER *****************//
// app.all('/*',
//       middleware.authentication // apply authentication for all api
// )

// ************ API PRIVATE ZONE ******************// 

// ************* Custom APIs ****************//

app.route('/logout')
   .get(authen.logout);

app.route('/refresh-token')
   .get(authen.refreshToken);
   
  //  app.route('/search')
  //  .post(controllers.api.generic.multiSearch)
   
   // *********** GENERIC APIS******************//

   app.route('/:model')
   .get(controllers.generic.get)
   .post(controllers.generic.post)
   .put(controllers.generic.put)

   app.route('/:model/:id')
   .get(controllers.generic.get)

   app.all('/**', function(req, res, next){res.status(404).send({code : 404, status : '404 endppoint not found'})})
module.exports = app
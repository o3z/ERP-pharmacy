var express = require('express')
var app = express.Router()
var middleware = require('./middleware');
var bodyParser = require('body-parser');
var Ngu = Negiup;
var importer = Ngu.importer(__dirname);
var controllers = importer('./../controllers/pharmacy');
var authen = importer('./../controllers/authen');

// ************ MIDDLEWARE *****************//
app.all('/*',
      middleware.apiResponse // standardize api response
      //   middleware.processRequest // process request queries 
);

// ************ API PUBLIC ZONE ******************// 
// app.route('/socket')
//       .post(controllers.api.socket.create)
// app.route('/login')
//       .post(authen.login);
app.route('/register/step1')
      .post(controllers.register.step1)
app.route('/register/step2')
      .post(controllers.register.step2)
app.route('/register/step3')
      .post(controllers.register.step3)

//************ SECURITY LAYER *****************//
// app.all('/*',
//       middleware.authentication // apply authentication for all api
// )

// ************ API PRIVATE ZONE ******************// 
app.route('/account/update/:id')
      .get(controllers.staff.get)
      .post(controllers.staff.update.update)
app.route('/warehouse/:model/:id')
      .get(controllers.warehouse.get)
      .post(controllers.warehouse.import)
app.route('/report/:model/:id').get(controllers.report.get)
app.route('/staff/:model/:id').get(controllers.staff.get)
app.route('/spharmacy/:model/:id').get(controllers.spharmacy.get)
app.route('/support/:model/:id').get(controllers.support.get)
app.route('/setting/:model/:id').get(controllers.setting.get)
app.route('/warehouse/:model').get(controllers.warehouse.get)
app.route('/report/:model').get(controllers.report.get)
app.route('/staff/:model').get(controllers.staff.get)
app.route('/spharmacy/:model').get(controllers.spharmacy.get)
app.route('/support/:model').get(controllers.support.get)
app.route('/setting/:model').get(controllers.setting.get)

      


      

// ************* Custom APIs ****************//

app.all('/**', function (req, res, next) { res.status(404).send({ code: 404, status: '404 endppoint not found' }) })
module.exports = app
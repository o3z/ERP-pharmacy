require('dotenv').config();
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var mongoose = require('mongoose');
var expressHbs = require('express-handlebars');

global.async = require('async');
global._ = require('lodash');
global.moment = require('moment');
global.config = require('config');
global.logger = require('./server/services/logger');
global.errorCode = require('./server/libs/errorCode');
let Negiup = require('./server/core/model');
global.Negiup = new Negiup();
global.Negiup.init()

// var api = require('./server/routes/api.router.js');
var admin = require('./server/routes/admin.router.js');
var customer = require('./server/routes/customer.router.js');
var pharmacy = require('./server/routes/pharmacy.router.js');
var site = require('./web/routes/erp-pharmacy.router');


/****************************
DATABASE CONNECTION
*****************************/
// mongoose.set('server', config.database.mongodb.config.server);
// mongoose.set('replset', config.database.mongodb.config.replset);
mongoose.connection.on('error', (err)=>{logger.error('[mongodb] unable to connect ', err)})
mongoose.connection.on('connect', ()=>{logger.info('[mongodb] connect ' + config.database.mongodb.url)})
mongoose.connection.on('disconnected', () =>{logger.error('[mongodb] disconnected')});
process.on('SIGINT', function() {  
	mongoose.connection.close(function () { 
		logger.error('[mongodb] connection disconnected through app termination'); 
		process.exit(0); 
	})
})
/*****************************
EXPRESS APP
*****************************/

// view engine setup
app.engine('.hbs', expressHbs({
	// defaultLayout: __dirname +'/web/template/home.hbs',
	layoutsDir: __dirname + '/web/template/layout',
	partialsDir: __dirname + '/web/template/partials',
	extname: '.hbs'
}));
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname,'/web/template'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/public', express.static(__dirname + '/web/public'));
app.all('*', function(req,res,next){
	res.header('Access-Control-Allow-Origin', '*'); // TODO: need enhanced security here
	res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-XSRF-TOKEN, token, cookies, Authorization")
	req.header('Accept-Language','*');
	req.headers['content-type'] = (req.headers['content-type'] == "application/x-www-form-urlencoded; charset=ISO-8859-1")? 'application/x-www-form-urlencoded; charset=utf-8': req.headers['content-type']
	if(req.method == 'OPTIONS') res.status(200).send()
	else  next();
})

/*************** 
APP ROUTERS 
******************/ 
app.use('/api/admin', admin);
app.use('/api/customer', customer);
app.use('/api/pharmacy', pharmacy);
// app.use('/api', api); 
app.use('/', site);

/*************************
STARTING APP
*************************/
mongoose.connect(process.env.DB || config.database.mongodb.url || config.database.mongodb.localhost , config.database.mongodb.config, function(err){
	if (!err) {
		var server = app.listen(process.env.PORT || config.port || 4444, function () {
			logger.info('*************** EXPRESS STARTED ON PORT ' + server.address().port + '****************')
			logger.info('*************** STARTING APP IN ' + process.env.NODE_ENV + ' mode *****************')
			
		});
	};
});

module.exports = app;

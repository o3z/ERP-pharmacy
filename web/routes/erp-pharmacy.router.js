var express = require('express');
var router = express.Router();
var Ngu = Negiup;
var middleware = Ngu.services.middleware;
var importer = Ngu.importer(__dirname);

controller = importer('./../controller');
  // landingPage = require('./../controller/landingPage.js');
  // post = require('./../controller/post.js');
  // home = require('./../controller/home.js');
router.get('/**', function (req, res, next) { 
  if(req.cookies.sessionId) next()
  else {
    newCookie = Ngu.services.web.cookie.encrypt(req.originalUrl)
    res.cookie('_ngu', newCookie, {expire: 7*24*60*60*1000 + Date.now()})
    next()
    // req.protocol + '://' + req.get('host') + req.originalUrl;
  }
 });
/* GET users listing. */
router.use("**", middleware.tracking.start);
router.get('/', controller.home);
/* GET users listing. */
router.get('/:category/:category1/:category2/:slug', controller.post);
router.get('/:category/:category1/:category2', controller.category);
router.get('/:category/:category1/:slug', controller.post);
router.get('/:category/:category1', controller.category);
router.get('/:category/:slug', controller.post);
router.get('/:category', controller.category);
router.use("**", middleware.tracking.end);
// router.get('/**', function (req, res, next) { res.redirect(302, '/') });

module.exports = router;

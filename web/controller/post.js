Ngu = Negiup

module.exports = function (req, res, next) {
    slug = req.params.slug
    cookie = req.cookies

    async.waterfall([
        function prePost(callback){
            logger.debug('[VIEW] post prePost')
            Ngu.model('Post')
            .findOne({
                // status: 'PUBLISHED',
                slug: slug
            })
            .lean()
            .exec(function(err, post){
                if(post) callback(false, post)
                else callback(true)
            })
        },
        function findParent(post, callback){
            logger.debug('[VIEW] post findParent')
            category = []
            if(req.params.category) category.push(req.params.category)
            if(req.params.category1) category.push(req.params.category1)
            if(req.params.category2) category.push(req.params.category2)
            parents = Ngu.services.categoryMenu.findParent(category[2]||category[1]||category[0])
            if(parents.some((parent, i)=>{ return category[i] != parent })) return callback(true)
            else {
                index = _.findIndex(Ngu.services.categoryMenu.getCategory(), (o)=>{ return o.slug == category[category.length - 1]})
                callback(false, {
                    post: post,
                    category: Ngu.services.categoryMenu.getCategory()[index],
                    parents: parents.map((o)=>{ return _.find(Ngu.services.categoryMenu.getCategory(), (ob)=>{ return o == ob.slug})})
                })
            }
        },
        function view(data, callback){
            async.parallel([
                function recommend(cb){
                    logger.debug('[VIEW] post recommend')
                    cookie._ngu = '5b127eb47202ac0864da8140s'
                    Ngu.services.web.recommend(cookie, function(err, recommend){
                        sort = null
                        if(err || !recommend) {
                            categories = Ngu.services.categoryMenu.findChild(data.category.slug)
                            categories.forEach((o)=>{
                                category = _.find(Ngu.services.categoryMenu.getCategory(), (ob)=>{ return o.slug == ob.slug})
                                o = _.merge(o, category) 
                            })
                            data.categoriesChild = categories
                            filter = {
                                category: { $in: data.categoriesChild.map((o)=>{return o._id})}
                            }
                            sort = '-vote'
                        } else {
                            filter = {
                                _id: { $in: recommend.data.result}
                            }
                        }
                        Ngu.model('Post')
                            .find(filter)
                            .limit(10)
                            .sort(sort)
                            .select('-content')
                            .lean()
                            .exec(function(err, posts){
                                if(posts.length > 0){
                                    data.recommend = posts
                                } else {
                                    data.recommend = []
                                }
                                cb(false, true)
                            })
                    })
                },
                function hot(cb) {
                    logger.debug('[VIEW] post hot')
                    Ngu.model('Post')
                        .find()
                        .limit(10)
                        .sort('-viewer')
                        .select('-content')
                        .lean()
                        .exec(function (err, posts) {
                            if (posts.length > 0) {
                                data.hot = posts
                            } else {
                                data.hot = []
                            }
                            cb(false, true)
                        })
                },function breadcum(cb){
                    logger.debug('[VIEW] POST breadcum')
                    data.parents.push(data.category)
                    cb(false, true)
                },
            ],function(err, results) {
                callback(err, data)
            })
            
        }
    ],function(err, result){
        if(err){
            res.render('error', {
                title: '404 PAGE NOT FOUND',
                data: result,
                layout: 'default'
            }, function(err, html) {
                res.status(200).send(html);
            })
        } else if(result.post){
            res.render('post', {
                title: result.post.meta.title,
                data: result,
                layout: 'default'
            }, function(err, html) {
                res.status(200).send(html);
            })
        } else {
            res.render('category', {
                title: result.category.name,
                data: result,
                layout: 'default'
            }, function(err, html) {
                res.status(200).send(html);
            })
        }
    })

}
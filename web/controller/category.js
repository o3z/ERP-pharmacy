var Ngu = Negiup

module.exports = function(req, res, next){
    cookie = req.cookies
    paging = req.query.page || 0
    current = paging
    async.waterfall([
        function findParent(callback){
            logger.debug('[VIEW] post findParent')
            category = []
            if(req.params.category) category.push(req.params.category)
            if(req.params.category1) category.push(req.params.category1)
            if(req.params.category2) category.push(req.params.category2)
            parents = Ngu.services.categoryMenu.findParent(category[2]||category[1]||category[0])
            if(parents.some((parent, i)=>{ return category[i] != parent })) return callback({error: true})
            else {
                index = _.findIndex(Ngu.services.categoryMenu.getCategory(), (o)=>{ return o.slug == category[category.length - 1]})
                if(index < 0) return callback({error: false})
                else callback(false, {
                    category: Ngu.services.categoryMenu.getCategory()[index],
                    parents: parents.map((o)=>{ return _.find(Ngu.services.categoryMenu.getCategory(), (ob)=>{ return o == ob.slug})})
                })
            }
        },
        function view(data, callback){
            async.parallel([
                function findAll(cb){
                    data.paging = paging
                    logger.debug('[VIEW] category findAll')
                    categories = Ngu.services.categoryMenu.findChild(data.category.slug)
                    categories.forEach((o)=>{
                        category = _.find(Ngu.services.categoryMenu.getCategory(), (ob)=>{ return o.slug == ob.slug})
                        o = _.merge(o, category) 
                    })
                    data.categoriesChild = categories
                    Ngu.model('Post')
                        .find({
                            category: { $in: categories.map((o)=>{return o._id})}
                        })
                        .skip(current*10)
                        .limit(10)
                        .sort('-createdAt')
                        .select('-content')
                        .lean()
                        .exec(function (err, posts) {
                            if (posts.length > 0) {
                                data.all = posts
                            } else {
                                data.all = []
                            }
                            cb(false, true)
                        })
                },
                function recommend(cb){
                    logger.debug('[VIEW] category recommend')
                    cookie._ngu = '5b127eb47202ac0864da8140'
                    Ngu.services.web.recommend(cookie, function(err, recommend){
                        sort = null
                        if(err) {
                            filter = {
                                category: { $in: data.categoriesChild.map((o)=>{return o._id})}
                            }
                            sort = '-viewer'
                        } else {
                            filter = {
                                _id: { $in: recommend.data.result}
                            }
                        }
                        Ngu.model('Post')
                            .find(filter)
                            .limit(10)
                            .sort(sort)
                            .select('-content')
                            .lean()
                            .exec(function(err, posts){
                                if(posts.length > 0){
                                    data.recommend = posts
                                } else {
                                    data.recommend = []
                                }
                                cb(false, true)
                            })
                    })
                },
                function hot(cb) {
                    logger.debug('[VIEW] category hot')
                    Ngu.model('Post')
                        .find()
                        .limit(10)
                        .sort('-viewer')
                        .select('-content')
                        .lean()
                        .exec(function (err, posts) {
                            if (posts.length > 0) {
                                data.hot = posts
                            } else {
                                data.hot = []
                            }
                            cb(false, true)
                        })
                },
                function findMenu(cb) {
                    logger.debug('[VIEW] category findMenu')
                    data.menu = Ngu.services.categoryMenu.getCategory().map((ob)=>{
                        if(ob.parent && (ob.parent.toString() == data.category._id.toString()))  return ob 
                        else return false
                    })
                    _.pull(data.menu , false)
                    cb(false, true)
                },
                function pagingMax(cb){
                    logger.debug('[VIEW] category pagingMax')
                    Ngu.model('Post')
                    .count({
                        category: { $in: data.categoriesChild.map((o)=>{return o._id})}
                    }, function (err, count) {
                        data.pagingMax = Math.floor(count/10 + 1)
                        if(paging <= data.pagingMax) cb(false, true)
                        else cb(true)
                    })
                },
                function breadcum(cb){
                    logger.debug('[VIEW] category breadcum')
                    data.parents.push(data.category)
                    cb(false, true)
                },
            ],function(err, results) {
                callback(err, data)
            })
        }
    ],function(err, result){
        if(err){
            if(err.error){
                res.render('error', {
                    title: '404 PAGE NOT FOUND',
                    data: result,
                    layout: 'default'
                }, function(err, html) {
                    res.status(200).send(html);
                })
            } else next()
        } else if(result.category) {
            res.render('category', {
                title: result.category.name,
                data: result,
                layout: 'default'
            }, function(err, html) {
                res.status(200).send(html);
            })
        } else next()
    })
}